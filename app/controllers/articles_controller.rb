class ArticlesController < ApplicationController

 impressionist :actions=>[:show]

  def index
    @entities = Article.all.page(params[:page]).per(params[:per_page])
  end

  def show
    @entities = {}
    @entities[:current] = Article.find(params[:id])
    @entities[:recommended] = recommended
  end

  def recommended
    Article.all.sample(2)
  end
end
