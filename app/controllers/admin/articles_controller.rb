class Admin::ArticlesController < Admin::CrudController

  def index
    @q = Article.distinct.search(params[:q])
    @entities = @q.result.page(params[:page]).per(params[:per_page])
  end

  private

  def set_class_name
    @class_name = 'Article'
  end

  def entity_params
    params.require(:article).permit(:title, :body, :image).tap do |param|
      param[:user] = current_user
    end
  end
end
