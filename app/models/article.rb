class Article < ApplicationRecord
  is_impressionable
  mount_uploader :image, ImageUploader
  belongs_to :user
  validates :title, :body, :image, presence: true
end
