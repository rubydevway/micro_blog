class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :articles

  after_create :ser_role

  def admin?
    self.has_role? :admin
  end

  def copywriter?
    self.has_role? :copywriter
  end

  def set_role
    self.add_role :copywriter
  end
end
