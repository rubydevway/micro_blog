Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'articles#index'

  get 'article/:id' => 'articles#show', as: 'article'

  namespace :admin do
    get '' => 'articles#index'
    resources :users
    resources :articles
  end
end
