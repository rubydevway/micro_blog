class ChangeOwnerIdColumn < ActiveRecord::Migration[5.0]
  def change
    rename_column :articles, :owner_id, :user_id
  end
end
